package main

import (
	"database/sql"
	"fmt"
	_ "github.com/bmizerany/pq"
	"github.com/gorilla/mux"
	"net/http"
	"os"
)

func main() {
	r := mux.NewRouter()

	db, err := sql.Open("postgres", "user=ddude dbname=lobbies"+
		"sslmode=verify-full")
	if err != nil {
		fmt.Println("Error Opening DB Connection!")
		os.Exit(1)
	}

	r.HandleFunc("/lobbies", LobbiesHandler)
	r.HandleFunc("/lobbies/{id:[0-9]+}", LobbyHandler)

	r.HandleFunc("/players", PlayersHandler)
	r.HandleFunc("/players/{id:[0-9]+}", PlayerHandler)

	http.Handle("/", r)
	http.ListenAndServe(":3333", nil)
}

// Handles requests related to all lobbies. This should only
// be used to return a list of all current lobbies. Lobby data
// will be returned as a JSON object, with the following format:
// TODO: Insert format example for lobbies JSON
func LobbiesHandler(w http.ResponseWriter, r *http.Request) {
	// get all lobby data, and send it back as json
	w.Header().Add("Content-Type", "text/html")
	// w.Header().Add("Content-Type", "application/json")
	fmt.Fprint(w, "Lobbies")
}

func LobbyHandler(w http.ResponseWriter, r *http.Request) {
	// get specific lobby data, return as json

	vars := mux.Vars(r)

	id := vars["id"] // lobby id

	w.Header().Add("Content-Type", "application/json")
	// get player data from our database, return it as json
	// response := <get lobby data using 'id'>
	response := "Lobby" + id
	fmt.Fprint(w, response)
}

func PlayersHandler(w http.ResponseWriter, r *http.Request) {
	// get all player data, and send it back as json
	w.Header().Add("Content-Type", "application/json")
	fmt.Fprint(w, "Players")
}

func PlayerHandler(w http.ResponseWriter, r *http.Request) {
	// get specific player data, and send it back as json

	vars := mux.Vars(r)

	id := vars["id"] // player id

	w.Header().Add("Content-Type", "application/json")

	// response := <get player data using 'id'>
	fmt.Fprint(w, "Player"+id)
}
